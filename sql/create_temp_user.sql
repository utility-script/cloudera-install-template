CREATE DATABASE scm DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
GRANT ALL ON scm.* TO 'scm'@'%' IDENTIFIED BY 'p@ssw0rdDB';

GRANT ALL ON *.* TO 'temp'@'%' IDENTIFIED BY 'p@ssw0rdDB' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'scm' IDENTIFIED BY 'p@ssw0rdDB';
FLUSH PRIVILEGES;
-- Auto generate SCM database with following command
-- /opt/cloudera/cm/schema/scm_prepare_database.sh mysql -h name-node01.cdh.suksan.cc -utemp -pP@ssw0rd --scm-host name-node01.cdh.suksan.cc rid_scm rid_scm_user