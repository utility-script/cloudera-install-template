ansible-playbook playbooks/01_prepare_centos/01_ntp-playbook.yml
ansible-playbook playbooks/01_prepare_centos/02_etchosts-playbook.yml
ansible-playbook playbooks/01_prepare_centos/03_SElinux-playbook.yml
ansible-playbook playbooks/01_prepare_centos/04_firewall-playbook.yml
ansible-playbook playbooks/01_prepare_centos/05_thp-playbook.yml
ansible-playbook playbooks/01_prepare_centos/06_swapness-playbook.yml

ansible-playbook playbooks/reboot.yml

ansible-playbook playbooks/02_repository_setup/01_install_apache.yml
ansible-playbook playbooks/02_repository_setup/02_setup_cloudear_package.yml
ansible-playbook playbooks/02_repository_setup/03_set_cdh_repository.yml

# ansible-playbook playbooks/03_compiler_setup/01_java_setup.yml
# ansible-playbook playbooks/03_compiler_setup/02_scala_setup.yml

# ansible-playbook playbooks/04_scm_install/01_install_cm.yml
# ansible-playbook playbooks/04_scm_install/02_install_worker.yml
# ansible-playbook playbooks/05_db_setup/01_mysql_setup.yml
# ansible-playbook playbooks/05_db_setup/03_install_mysql_connector.yml
# set secure

# ansible-playbook playbooks/06_scm_start/01_scm_start.yml
# ansible-playbook playbooks/06_scm_start/02_agent_start.yml
